(function($){

	/* ---------------- Back to TOP ------------------ */
	$(window).on('scroll', function(e){
		if ($(window).scrollTop() > 700){
			$('#hacia-arriba').css('bottom', '70px');
		}else{
			$('#hacia-arriba').css('bottom', '-70px');
		}
	});

	$('#hacia-arriba').on('click', function(e){
		var body = $("html, body");
		body.animate({scrollTop:0}, 1100, 'swing');
	});
	/* ---------------- #FIN Back to TOP ------------------ */

	$('#js-soy').on('click', function(){
		var body = $("html, body");
		body.animate({scrollTop: $('[data-quien="quien-soy"]').offset().top}, 500, 'swing');
	});

	$('#js-creaciones').on('click', function(){
		var body = $("html, body");
		body.animate({scrollTop: $('[data-quien="mis-creaciones"]').offset().top}, 700, 'swing');
	});

	$('#js-cv').on('click', function(){
		var body = $("html, body");
		body.animate({scrollTop: $('[data-quien="cv"]').offset().top}, 700, 'swing');
	});

	$('#js-contactame').on('click', function(){
		var body = $("html, body");
		body.animate({scrollTop: $('[data-quien="contactame"]').offset().top}, 800, 'swing');
	});


})(jQuery)();