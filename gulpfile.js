	var gulp= require('gulp'),
	connect = require('gulp-connect'),
	historyApiFallback = require('connect-history-api-fallback');

	var concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	minifyCss = require('gulp-minify-css'),
	minifyHTML = require('gulp-minify-html');

	var autoprefix_plugin = require('less-plugin-autoprefix');
	var autoprefix_options = {browsers: ["last 3 versions"]};
	var autoprefix = new autoprefix_plugin(autoprefix_options);

	var groupMediaQueries = require('less-plugin-group-css-media-queries');
	var less = require('gulp-less');

	var rename = require('gulp-rename');

	var LessPluginCleanCSS = require('less-plugin-clean-css');
	cleanCSSPlugin = new LessPluginCleanCSS({advanced: true});


	gulp.task('less',function(){
		gulp.src('./app/stylesheets/style.less')
	    .pipe(less({
	        plugins: [autoprefix]
	    }))
	    .pipe(rename('style.css'))
	    .pipe(minifyCss({compatibility: 'ie8'}))
	    .pipe(gulp.dest('./app/css'))
	    .pipe(connect.reload());
	});

	
	// Servidor web de desarrollo
	gulp.task('server', function() {
		connect.server({
			root: 'app',
			hostname: '0.0.0.0',
			port: 8080,
			livereload: true,
			/*middleware: function(connect, opt) {
				return [ historyApiFallback ];
			}*/
		});
	});


	// Recarga el navegador cuando hay cambios en el HTML
	gulp.task('html', function() {
		gulp.src('./app/**/*.html')
		.pipe(connect.reload());
	});

	// Vigila cambios que se produzcan en el código
	// y lanza las tareas relacionadas
	gulp.task('watch', function() {
		gulp.watch(['./app/*.html'], ['html']);
		gulp.watch(['./app/stylesheets/**/*.less'], ['less']);
	});
	gulp.task('default', ['server', 'watch']);